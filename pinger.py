from win32.lib import win32serviceutil
from win32 import win32service, win32event, servicemanager

import platform    # For getting the operating system name
import subprocess  # For executing a shell command

import time
import logging
import logging.handlers
import sys
import os

from pathlib import Path

LOG_PATH = Path.home() / ".pinger"
os.makedirs(LOG_PATH, exist_ok=True)

# create logger with 'spam_application'
logger = logging.getLogger('pinger')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.handlers.RotatingFileHandler(LOG_PATH / "pinger.log", maxBytes=2**30, backupCount=30)
fh.setLevel(logging.INFO)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', "%Y-%m-%dT%H:%M:%S%z")
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

class Pinger(win32serviceutil.ServiceFramework):
    _svc_name_ = 'Pinger'
    _svc_display_name_ = 'Pinger Service'
    _svc_description_ = 'Logs when a ping cannot be made to a host'

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
    
    def SvcStop(self):
        self.stop()
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        self.start()
        # Write a stop message.
        servicemanager.LogMsg(
                servicemanager.EVENTLOG_INFORMATION_TYPE,
                servicemanager.PYS_SERVICE_STOPPED,
                (self._svc_name_, '')
                )
        self.main()

    def start(self):
        self.isrunning = True

    def stop(self):
        self.isrunning = False

    @staticmethod
    def ping(host):
        """
        Returns True if host (str) responds to a ping request.
        Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
        """

        # Building the command. Ex: "ping -c 1 google.com"
        command = ['ping', '-n', '1', '-i', '1', host]

        return subprocess.call(command, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL) == 0

    def main(self):
        while self.isrunning:
            if self.ping('google.com'):
                logger.info("Ping successful")
            else:
                logger.error("Ping unsuccessful; probably no internet")
            time.sleep(30)


if __name__=='__main__':
    win32serviceutil.HandleCommandLine(Pinger)
