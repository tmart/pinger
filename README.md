# Pinger

My ISP drops my connection pretty frequently. I wanted to have more data when I call them next than just saying "it happens sporadically!". This is an attempt at making that phone call more concrete.

This project is a Windows service that pings `google.com` rougly every 30 seconds and logging the result to `$HOME/.pinger/pinger.log`. By making it a service, we don't have to think about it.

## TODO

- It appears this doesn't really work after restarts. Requires manual start/stop from Services list? Figure this out.

## Prerequisites

1. Install Python 3

2. Install `pywin32` as

    Follow instructions at https://github.com/mhammond/pywin32, namely:

    ```bash
    pip install pywin32
    cd <to python install directory>
    python Scripts/pywin32_postinstall.py -install
    ```

    You need to do the last step as an administrator on the system. I usually do this by right-clicking my terminal program and clicking "Run as administrator".


## Installation

1. Install and start this service
   
   ```bash
   cd <this repo dir>
   python pinger.py --startup auto install
   python pinger.py start
   ```

2. (Optional) Ensure the service is in the Services list
    
    1. Press the Windows key
    2. Start typing `services` and look for the "Services" app and select it.
    3. Find "Pinger" service in the list.
    4. Ensure it's set for Automatic startup, browse Properties, etc.

## Development

1. Do some hacking

2. Stop the service
   
   ```bash
   python pinger.py stop
   ```

3. Update the service

    ```bash
    python pinger.py --startup auto update
    ```

4. Start the service again

    ```bash
    python pinger.py start
    ```

5. (Optional) See how things are going

    ```bash
    python pinger.py debug
    ```

    If there are problems, you might see a traceback or other logging if you're using a logger.

## Notes/Caveats

- The output log path is hardcoded. For now, this file will be at `$HOME/.pinger/pinger.log`.
- BIG: If this service starts too early, like before networking is established, the log will report false positives. You may be able
  to ascertain these instances by looking to see that a longer than usual delay has occurred since the last report because of a reboot.

## Inspiration/Guides
- https://www.thepythoncorner.com/2018/08/how-to-create-a-windows-service-in-python/
- https://gist.github.com/mastro35/28d77339656b92d14e7a020367717b2f
- https://souluran.wordpress.com/2016/07/19/python-script-as-windows-service-on-schedule-logging/